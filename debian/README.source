kmer-tools for Debian
---------------------

Bundled Dependencies
````````````````````
This package uses the mt19937ar random number generator [1] and includes a
copy in the source distribution in libutils/mt19937ar. It is a modified
version, but the vanilla package does not appear to be available in Debian
either. The developers of mt19937ar have a whole suite of
implementations [2-3], and only some of them appear to be in Debian. For now,
the bundled copy has been left alone and is used to build this package.

1. http://www.math.sci.hiroshima-u.ac.jp/~m-mat/MT/MT2002/emt19937ar.html
2. http://www.math.sci.hiroshima-u.ac.jp/~m-mat/MT/emt.html
3. http://www.math.sci.hiroshima-u.ac.jp/~m-mat/MT/VERSIONS/eversions.html

This package also bundles a (modified) copy of kazlib.
In future revisions of the software, upstream prefers to drop the dependency
on kazlib and use the C++ STL instead [4]. Thus, there is not really a good
reason to try to modify the software to use the Debian-packaged kazlib in
the meantime.

4. https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=784863#20

Monolithic Source Package
`````````````````````````
Although this package builds several loosely-related binary packages,
it is better that they are all built from this one source package
instead of creating one source package for each component (using upstream's
guidelines in the PACKAGING file, or his separated tarballs). They share
dependencies on some libraries that cannot be readily packaged properly
(shared, with proper sonames) on their own.

See also upstream's comment on this issue:
https://lists.debian.org/debian-med/2015/05/msg00075.html

 -- Afif Elghraoui <afif@ghraoui.name>, Sat,  2 Jan 2016 16:40:29 -0800

